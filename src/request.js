const req = require('request-promise');
const format = require('dateformat');
const sign = require('./sign');
const {DEFAULT_SERVER} = require('./constants');

module.exports = async (method, param_json, version = '1.0', access_token = '', app_key = '', app_secret = '') => {
    const qs = {
        method,
        v: version,
        access_token,
        app_key,
        timestamp: format(new Date(), 'yyyy-mm-dd HH:MM:ss'),
        sign_method: 'md5',
        format: 'json',
        '360buy_param_json': param_json
    };
    qs.sign = sign(qs, app_key, app_secret);
    console.log(qs);
    return await req({
        uri: DEFAULT_SERVER, qs, json: true
    });
};
